package com.example.happytraveladmin.viewmodel;

        import androidx.arch.core.util.Function;
        import androidx.lifecycle.LiveData;
        import androidx.lifecycle.Transformations;
        import androidx.lifecycle.ViewModel;

        import com.example.happytraveladmin.data.FirebaseQueryLiveData;
        import com.example.happytraveladmin.model.User;
        import com.google.firebase.database.DataSnapshot;
        import com.google.firebase.database.DatabaseReference;
        import com.google.firebase.database.FirebaseDatabase;
        import java.util.ArrayList;
        import java.util.List;

public class UserViewmodel extends ViewModel {
        private static final DatabaseReference HOT_STOCK_REF =
                FirebaseDatabase.getInstance().getReference("/users");
        private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOT_STOCK_REF);

        private List<User> mList = new ArrayList<>();
        private final LiveData<List<User>> userListLivedata = Transformations.map(liveData, new DeserializerList());

        public LiveData<List<User>> getUserListLivedata() {
                return userListLivedata;
        }

        private class DeserializerList implements Function<DataSnapshot, List<User>> {
                @Override
                public List<User> apply(DataSnapshot input) {
                        mList.clear();
                        for (DataSnapshot snap : input.getChildren()) {
                                User hotStock = snap.getValue(User.class);
                                mList.add(hotStock);
                        }
                        return mList;
                }
        }
}
