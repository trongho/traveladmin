package com.example.happytraveladmin.viewmodel;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.happytraveladmin.data.FirebaseQueryLiveData;
import com.example.happytraveladmin.model.Hotel;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HotelViewModel extends ViewModel {
    private static final DatabaseReference HOTEL_REF = FirebaseDatabase.getInstance().getReference("/hotels");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOTEL_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private List<Hotel> mList = new ArrayList<>();
    private final LiveData<List<Hotel>> hotelListLivedata = Transformations.map(liveData, new DeserializerList());
    public LiveData<List<Hotel>> getHotelListLivedata() {
        return hotelListLivedata;
    }
    private class DeserializerList implements Function<DataSnapshot, List<Hotel>> {
        @Override
        public List<Hotel> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
//                if(snap.child("userId").getValue().toString().equalsIgnoreCase("hi")) {
                    Hotel hotel = snap.getValue(Hotel.class);
                    mList.add(hotel);
//                }
            }
            return mList;
        }
    }


    public MutableLiveData<String> imageUrl=new MutableLiveData<>();
    public MutableLiveData<String> hotelName=new MutableLiveData<>();
    public MutableLiveData<String> provine=new MutableLiveData<>();
    public MutableLiveData<String> district=new MutableLiveData<>();
    public MutableLiveData<String> standardStar=new MutableLiveData<>();
    public void getHotelDetail(String uid){
        HOTEL_REF.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Hotel hotel=new Hotel();
                hotel = snapshot.getValue(Hotel.class);
                imageUrl.setValue(hotel.getImageUrl());
                hotelName.setValue(hotel.getHotelName());
                provine.setValue(hotel.getProvine());
                district.setValue(hotel.getDistrict());
                standardStar.setValue(hotel.getStandardStar());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private final MutableLiveData<Boolean> hotelUpdateIsSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getHotelUpdateIsSuccessful() {
        return hotelUpdateIsSuccessful;
    }

    public void updateHotel(String uid) {
        Hotel hotel=new Hotel();
        Task uploadTask = HOTEL_REF
                .child(uid)
                .setValue(user);
        uploadTask.addOnSuccessListener(o ->hotelUpdateIsSuccessful.setValue(true));
    }


}
