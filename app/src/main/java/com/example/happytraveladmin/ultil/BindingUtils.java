package com.example.happytraveladmin.ultil;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseMethod;

import com.example.happytraveladmin.R;
import com.example.happytraveladmin.model.User;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

public class BindingUtils {

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imgurl) {
        Picasso.get().load(imgurl).placeholder(R.drawable.ic_image_black_24dp).into(view);
    }

    @BindingAdapter("app:errorText")
    public static void setErrorMessage(TextInputLayout view, String errorMessage) {
        view.setError(errorMessage);
    }

}
