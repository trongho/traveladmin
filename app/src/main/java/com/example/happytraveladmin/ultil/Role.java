package com.example.happytraveladmin.ultil;

public enum Role {
    USER,
    ADMIN,
    ADMIN_HOTEL,
    RECEPTION
}
