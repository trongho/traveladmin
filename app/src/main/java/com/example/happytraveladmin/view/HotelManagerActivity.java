package com.example.happytraveladmin.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.happytraveladmin.Adapter.HotelAdapter;
import com.example.happytraveladmin.Adapter.UserAdapter;
import com.example.happytraveladmin.R;
import com.example.happytraveladmin.databinding.ActivityHotelManagerBinding;
import com.example.happytraveladmin.model.Hotel;
import com.example.happytraveladmin.model.User;
import com.example.happytraveladmin.viewmodel.HotelViewModel;
import com.example.happytraveladmin.viewmodel.UserViewmodel;

import java.util.List;

public class HotelManagerActivity extends AppCompatActivity {
    ActivityHotelManagerBinding binding;
    HotelViewModel hotelViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_hotel_manager);
        hotelViewModel= ViewModelProviders.of(this).get(HotelViewModel.class);
        binding.setLifecycleOwner(this);

        getSupportActionBar().setTitle("Danh sách hotel");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initRecycleview();
    }

    private void initRecycleview(){
        LiveData<List<Hotel>> listLiveData=hotelViewModel.getHotelListLivedata();
        listLiveData.observe(this, new Observer<List<Hotel>>() {
            @Override
            public void onChanged(List<Hotel> hotels) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                HotelAdapter adapter=new HotelAdapter(hotels, new HotelAdapter.ItemClickListener() {
                    @Override
                    public void onClick(Hotel hotel) {
                        //item click
                    }
                }, new HotelAdapter.ItemLongClickListener() {
                    @Override
                    public boolean onLongClick(Hotel hotel) {
                        //item long ckick
                        return false;
                    }
                });
                binding.recyclerview.setLayoutManager(layoutManager);
                binding.recyclerview.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
