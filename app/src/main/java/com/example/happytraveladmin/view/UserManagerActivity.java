package com.example.happytraveladmin.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.widget.Toast;

import com.example.happytraveladmin.Adapter.UserAdapter;
import com.example.happytraveladmin.R;
import com.example.happytraveladmin.databinding.ActivityUserManagerBinding;
import com.example.happytraveladmin.model.User;
import com.example.happytraveladmin.viewmodel.LoginViewModel;
import com.example.happytraveladmin.viewmodel.UserViewmodel;

import java.util.List;

public class UserManagerActivity extends AppCompatActivity {
    ActivityUserManagerBinding binding;
    UserViewmodel userViewmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_user_manager);
        userViewmodel= ViewModelProviders.of(this).get(UserViewmodel.class);
        binding.setLifecycleOwner(this);

        getSupportActionBar().setTitle("Danh sách user");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initRecycleview();
    }

    private void initRecycleview(){
        LiveData<List<User>> listLiveData=userViewmodel.getUserListLivedata();
        listLiveData.observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                UserAdapter adapter=new UserAdapter(users, new UserAdapter.ItemClickListener() {
                    @Override
                    public void onClick(User user) {
                        //item click
                        Toast.makeText(getApplicationContext(),user.getFullname().toString(),Toast.LENGTH_SHORT).show();
                    }
                }, new UserAdapter.ItemLongClickListener() {
                    @Override
                    public boolean onLongClick(User user) {
                        //item long ckick
                        return false;
                    }
                });
                binding.recyclerview.setLayoutManager(layoutManager);
                binding.recyclerview.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }
}
