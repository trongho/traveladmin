package com.example.happytraveladmin.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.happytraveladmin.R;
import com.example.happytraveladmin.databinding.ActivityLoginBinding;
import com.example.happytraveladmin.viewmodel.LoginViewModel;

public class LoginActivity extends AppCompatActivity {
    ActivityLoginBinding binding;
    LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_login);
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setLoginviewmodel(loginViewModel);

        loginViewModel.getUserLoginIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            } else {
                Toast.makeText(getApplicationContext(), "Login fail", Toast.LENGTH_LONG).show();
            }
        });
    }
}
