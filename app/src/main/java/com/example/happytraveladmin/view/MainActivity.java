package com.example.happytraveladmin.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.happytraveladmin.R;
import com.example.happytraveladmin.databinding.ActivityMainBinding;
import com.example.happytraveladmin.viewmodel.LoginViewModel;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    LoginViewModel loginViewModel;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_main);
        loginViewModel= ViewModelProviders.of(this).get(LoginViewModel.class);

        //toolbar
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.toolbar_main);

        binding.content.usermanager.setOnClickListener(v->{
            startActivity(new Intent(getApplicationContext(),UserManagerActivity.class));
        });

    }
}
