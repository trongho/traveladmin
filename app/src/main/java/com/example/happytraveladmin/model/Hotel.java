package com.example.happytraveladmin.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.example.happytraveladmin.ultil.PaymentMethods;

import java.util.Map;

public class Hotel extends BaseObservable {
    private String hotelName;
    private String imageUrl;
    private String provine;
    private String district;
    private String description;
    private String address;
    private String standardStar;
    private String typeHotel;
    private Map<String, Boolean> utilities;
    private PaymentMethods paymentMethods;
    private String userId;


    public Hotel(String hotelName, String imageUrl, String provine, String district, String description, String address, String standardStar, String typeHotel, Map<String,Boolean> utilities, PaymentMethods paymentMethods, String userId) {
        this.hotelName = hotelName;
        this.imageUrl = imageUrl;
        this.provine = provine;
        this.district = district;
        this.description = description;
        this.address = address;
        this.standardStar = standardStar;
        this.typeHotel = typeHotel;
        this.utilities = utilities;
        this.paymentMethods = paymentMethods;
        this.userId = userId;
    }

    public Hotel() {
    }

    @Bindable
    public String getHotelName() {
        return hotelName;
    }

    public void setHotelName(String hotelName) {
        this.hotelName = hotelName;
        notifyPropertyChanged(BR.hotelName);
    }

    @Bindable
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    @Bindable
    public String getProvine() {
        return provine;
    }

    public void setProvine(String provine) {
        this.provine = provine;
        notifyPropertyChanged(BR.provine);
    }

    @Bindable
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
        notifyPropertyChanged(BR.district);
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    @Bindable
    public String getStandardStar() {
        return standardStar;
    }

    public void setStandardStar(String standardStar) {
        this.standardStar = standardStar;
        notifyPropertyChanged(BR.standardStar);
    }

    @Bindable
    public String getTypeHotel() {
        return typeHotel;
    }

    public void setTypeHotel(String typeHotel) {
        this.typeHotel = typeHotel;
        notifyPropertyChanged(BR.typeHotel);
    }

    @Bindable
    public Map<String, Boolean> getUtilities() {
        return utilities;
    }

    public void setUtilities(Map<String, Boolean> utilities) {
        this.utilities = utilities;
        notifyPropertyChanged(BR.utilities);
    }

    @Bindable
    public PaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    public void setPaymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
        notifyPropertyChanged(BR.paymentMethods);
    }

    @Bindable
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
        notifyPropertyChanged(BR.userId);
    }
}
